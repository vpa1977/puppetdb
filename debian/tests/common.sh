STATUS_URL="http://localhost:8080/status/v1/services/puppetdb-status"
QUERY_URL="http://localhost:8080/pdb/query/v4"

set -e

cleanup () {
	retval=$?
	set +e
	if [ -n "${AUTOPKGTEST_ARTIFACTS}" ]; then
		if [ -f /var/log/puppetdb/puppetdb.log ]; then
			cp /var/log/puppetdb/puppetdb.log "${AUTOPKGTEST_ARTIFACTS}"
		fi
		if [ -f /var/log/puppetdb/puppetserver.log ]; then
			cp /var/log/puppetdb/puppetserver.log "${AUTOPKGTEST_ARTIFACTS}"
		fi
		if [ -d /run/systemd/system ]; then
			journalctl -u puppetdb.service > "${AUTOPKGTEST_ARTIFACTS}/journal.log"
			if systemctl --no-pager list-unit-files puppetserver.service > /dev/null; then
				journalctl -u puppetserver.service > "${AUTOPKGTEST_ARTIFACTS}/journal.log"
			fi
		fi
	fi
	return $retval
}

get_status () {
	curl -s "$STATUS_URL"
}

check_status_var () {
	get_status | jq -e ".status[\"$1\"] == $2" >/dev/null 2>&1
}

query () {
	curl -s "${QUERY_URL}/$1"
}
